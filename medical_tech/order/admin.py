# Django
from django.contrib import admin

# PyPi: django-parler
from parler.admin import TranslatableAdmin

# Project
from medical_tech.order.models import Product, Order, OrderProduct


@admin.register(Product)
class ProductAdmin(TranslatableAdmin):
    list_display = ["id", "name", "quantity", "price"]


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ["id", "client", "employee", "total_price", "created_at"]


@admin.register(OrderProduct)
class OrderProductAdmin(admin.ModelAdmin):
    list_display = ["id", "order", "product", "price", "amount"]
