# Django
from django.db import models
from django.utils.translation import gettext as _

# Pypi: django-parler
from parler.models import TranslatableModel, TranslatedFields

# Project
from medical_tech.core.models import BaseDateModel
from medical_tech.client.models import Client
from medical_tech.employee.models import Employee


class Product(BaseDateModel, TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(_("name"), max_length=200)
    )
    quantity = models.IntegerField(_("quantity"))
    price = models.DecimalField(_("price"), max_digits=18, decimal_places=2)


class Order(BaseDateModel):
    client = models.ForeignKey(
        Client,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="orders"
    )
    employee = models.ForeignKey(
        Employee,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="orders",
    )
    total_price = models.DecimalField(_("total price"), max_digits=18, decimal_places=2)


class OrderProduct(BaseDateModel):
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name="order_products"
    )
    product = models.ForeignKey(
        Product,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="order_products"
    )
    price = models.DecimalField(_("sold price"), max_digits=18, decimal_places=2)
    amount = models.IntegerField(_("amount"), default=1)
