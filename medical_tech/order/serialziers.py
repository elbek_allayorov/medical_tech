# Django
from django.db import transaction
from django.utils.translation import gettext as _

# DRF
from rest_framework import serializers

# Project
from medical_tech.client.serializers import ClientSerializer
from medical_tech.employee.serializers import EmployeeSerializer
from medical_tech.order.models import OrderProduct, Product, Order


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            "id",
            "name",
            "quantity",
            "price",
        )


class OrderProductSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        self.fields["product"] = ProductSerializer()
        return super().to_representation(instance)

    class Meta:
        model = OrderProduct
        fields = (
            "id",
            "product",
            "price",
            "amount"
        )


class OrderSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        self.fields["client"] = ClientSerializer()
        self.fields["employee"] = EmployeeSerializer()
        return super().to_representation(instance)

    order_products = OrderProductSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            "id",
            "client",
            "employee",
            "total_price",
            "created_at",
            "order_products",
        )
        read_only_fields = (
            "total_price",
        )

    def validate(self, attrs):
        order_products = attrs["order_products"]
        for order_product in order_products:
            if order_product["amount"] > order_product["product"].quantity:
                raise serializers.ValidationError(_("Product amount is not available in stock"))
        return attrs
    
    def create(self, validated_data):
        order_products = validated_data.pop("order_products", [])
        total_price = sum([op["amount"] * op["price"] for op in order_products])
        with transaction.atomic():
            order = Order.objects.create(
                total_price=total_price,
                **validated_data
            )
            order_product_list = []
            for order_product in order_products:
                order_product_list.append(
                    OrderProduct(
                        order=order,
                        **order_product
                    )
                )
                product = order_product["product"]
                product.quantity -= order_product["amount"]
                product.save()
            OrderProduct.objects.bulk_create(
                order_product_list
            )
        return order
