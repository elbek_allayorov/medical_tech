# DRF
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from medical_tech.order.filters import OrderFilter
# Project
from medical_tech.order.models import Order
from medical_tech.order.serialziers import OrderSerializer


class OrderView(
    GenericViewSet,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filterset_class = OrderFilter
