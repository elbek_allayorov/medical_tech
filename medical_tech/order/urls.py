# Django
from django.urls import path, include

# Django Rest Framework
from rest_framework.routers import DefaultRouter

# Project
from medical_tech.order.views import OrderView

router = DefaultRouter()
router.register('order', OrderView, "order")

urlpatterns = [
    path('', include(router.urls))
]
