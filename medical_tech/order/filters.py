# PyPi: django_filters
import django_filters

# Project
from medical_tech.order.models import Order


class OrderFilter(django_filters.FilterSet):
    year = django_filters.NumberFilter(field_name="created_at", method="year_filter")
    month = django_filters.NumberFilter(field_name="created_at", method="month_filter")

    class Meta:
        model = Order
        fields = (
            "client",
            "employee",
            "year",
            "month"
        )

    def year_filter(self, queryset, name, value, *args, **kwargs):
        if value:
            lookup = '__'.join([name, 'year'])
            return queryset.filter(**{lookup: value})
        return queryset

    def month_filter(self, queryset, name, value, *args, **kwargs):
        if value:
            lookup = '__'.join([name, 'month'])
            return queryset.filter(**{lookup: value})
        return queryset
