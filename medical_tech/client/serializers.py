# DRF
from rest_framework import serializers

# Project
from medical_tech.client.models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class ClientResponseSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    full_name = serializers.CharField(max_length=256)
    products_count = serializers.IntegerField()
    sales_amount = serializers.DecimalField(max_digits=18, decimal_places=2)