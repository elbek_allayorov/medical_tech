# Django
from django.urls import path, include

# Django Rest Framework
from rest_framework.routers import DefaultRouter

# Project
from medical_tech.client.views import StatisticsClientView

router = DefaultRouter()
router.register(r"statistics/client", StatisticsClientView)

urlpatterns = [
    path('', include(router.urls))
]
