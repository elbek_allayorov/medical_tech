# Django
from django.contrib import admin

# Project
from medical_tech.client.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ["id", "full_name", "birthdate"]
