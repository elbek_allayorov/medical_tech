# Django
from django.db.models import Sum, Count
from django.utils.translation import gettext as _

# DRF
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

# PyPi: drf-yasg
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

# Project
from medical_tech.client.models import Client
from medical_tech.client.serializers import ClientResponseSerializer
from medical_tech.order.filters import OrderFilter

month_param = openapi.Parameter(
    'month',
    openapi.IN_QUERY,
    description="Filter sales my month",
    type=openapi.TYPE_NUMBER
)
year_param = openapi.Parameter(
    'year',
    openapi.IN_QUERY,
    description="Filter sales my year",
    type=openapi.TYPE_NUMBER
)
client_response = openapi.Response(
    'response description', ClientResponseSerializer
)


class StatisticsClientView(
    mixins.RetrieveModelMixin,
    GenericViewSet
):
    queryset = Client.objects.all()

    @swagger_auto_schema(
        operation_description=_("One Client Statistics"),
        manual_parameters=[month_param, year_param],
        responses={200: client_response}
    )
    def retrieve(self, request, *args, **kwargs):
        client = self.get_object()
        orders = client.orders.all()
        filtered_qs = OrderFilter(request.GET, queryset=orders).qs

        client.products_count = filtered_qs.aggregate(
            products_count=Count("order_products")
        ).get("products_count")
        client.sales_amount = filtered_qs.aggregate(
            sales_amount=Sum("total_price")
        ).get("sales_amount")

        serializer = ClientResponseSerializer()
        serializer.instance = client
        return Response(serializer.data)
