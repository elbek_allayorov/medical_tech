# Django
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

# Project
from medical_tech.core.swagger import urlpatterns as doc_urls
urlpatterns = doc_urls

urlpatterns += [
    path('admin/', admin.site.urls),
    path('<version>/', include("medical_tech.order.urls")),
    path('<version>/', include("medical_tech.employee.urls")),
    path('<version>/', include("medical_tech.client.urls")),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
