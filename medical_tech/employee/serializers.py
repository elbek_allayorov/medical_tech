# DRF
from rest_framework import serializers

# Project
from medical_tech.employee.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = '__all__'


class EmployeeResponseSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    full_name = serializers.CharField(max_length=256)
    clients_count = serializers.IntegerField()
    products_count = serializers.IntegerField()
    sales_amount = serializers.DecimalField(max_digits=18, decimal_places=2)
