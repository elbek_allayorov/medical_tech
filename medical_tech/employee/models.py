# Django
from django.db import models
from django.utils.translation import gettext as _


class Employee(models.Model):
    full_name = models.CharField(_("full name"), max_length=256)
    birthdate = models.DateField(_("date of birth"), null=True, blank=True)

    def __str__(self):
        return self.full_name
