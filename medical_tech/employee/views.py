# Python
from decimal import Decimal

# Django
from django.db.models import (
    Sum, Count, OuterRef, Subquery, IntegerField, DecimalField)
from django.db.models.functions import Coalesce
from django.utils.translation import gettext as _

# DRF
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

# PyPi: drf-yasg
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

# Project
from medical_tech.employee.models import Employee
from medical_tech.employee.serializers import EmployeeResponseSerializer
from medical_tech.order.filters import OrderFilter
from medical_tech.order.models import Order

month_param = openapi.Parameter(
    'month',
    openapi.IN_QUERY,
    description="Filter sales my month",
    type=openapi.TYPE_NUMBER
)
year_param = openapi.Parameter(
    'year',
    openapi.IN_QUERY,
    description="Filter sales my year",
    type=openapi.TYPE_NUMBER
)
employee_response = openapi.Response(
    'response description', EmployeeResponseSerializer
)


class StatisticsEmployeeView(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet
):
    queryset = Employee.objects.all()
    serializer_class = EmployeeResponseSerializer

    @swagger_auto_schema(
        operation_description=_("One Employee Statistics"),
        manual_parameters=[month_param, year_param],
        responses={
            200: employee_response,
        }
    )
    def retrieve(self, request, *args, **kwargs):
        employee = self.get_object()
        orders = employee.orders.all()
        filtered_qs = OrderFilter(request.GET, queryset=orders).qs

        employee.clients_count = filtered_qs.filter(
            client__isnull=False
        ).distinct("client").order_by().count()
        employee.products_count = filtered_qs.aggregate(
            products_count=Count("order_products")
        ).get("products_count")
        employee.sales_amount = filtered_qs.aggregate(
            sales_amount=Sum("total_price")
        ).get("sales_amount")

        serializer = EmployeeResponseSerializer()
        serializer.instance = employee
        return Response(serializer.data)

    @swagger_auto_schema(
        operation_description=_("All Employee Statistics"),
        manual_parameters=[month_param, year_param],
    )
    def list(self, request, *args, **kwargs):
        orders = Order.objects.filter(employee=OuterRef('pk'))
        filtered_qs = OrderFilter(request.GET, queryset=orders).qs
        queryset = self.queryset.annotate(
            clients_count=Coalesce(Subquery(
                    filtered_qs.order_by().filter(
                        client__isnull=False
                    ).select_related(
                        "client"
                    ).values(
                        "employee"
                    ).annotate(
                        count=Count("client",  distinct=True),
                    ).values("count"), output_field=IntegerField()
            ), 0),
            products_count=Coalesce(Subquery(
                    filtered_qs.order_by().values(
                        "employee"
                    ).annotate(
                        products_count=Count("order_products")
                    ).values("products_count"), output_field=IntegerField(),
            ), 0),
            sales_amount=Coalesce(Subquery(
                    filtered_qs.order_by().values(
                        "employee"
                    ).annotate(
                        sales_amount=Sum("total_price")
                    ).values("sales_amount"), output_field=DecimalField(),
            ), Decimal(0)),
        )
        page = self.paginate_queryset(queryset)
        serializer = EmployeeResponseSerializer(
            page, many=True
        )
        return self.get_paginated_response(serializer.data)
