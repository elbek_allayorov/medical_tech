# Django
from django.urls import path, include

# Django Rest Framework
from rest_framework.routers import DefaultRouter

# Project
from medical_tech.employee.views import StatisticsEmployeeView

router = DefaultRouter()
router.register(r"statistics/employee", StatisticsEmployeeView)

urlpatterns = [
    path('', include(router.urls))
]
