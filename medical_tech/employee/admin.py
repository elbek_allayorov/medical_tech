# Django
from django.contrib import admin

# Project
from medical_tech.employee.models import Employee


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ["id", "full_name", "birthdate"]
